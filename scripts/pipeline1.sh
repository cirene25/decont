
echo "Descontaminación de muestras"


echo "Descargando  "

wget -r -l1 -t1 -nd -N -np -A.fastq.gz -erobots=off -i ./decont/data/urls.txt


echo "Descargando fichero de descontaminación"

# Download the contaminants fasta file, and uncompress it
bash scripts/download1.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes


# Index the contaminants file
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx


# Merge the samples into a single file
for sid in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed "s:data/::" | sort | uniq) #TODO
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done


mkdir -p out/cutadapt
mkdir -p log/cutadapt

echo 'eliminando adaptadores'

for sample in out/merged/*
do
    sample=$(basename ${sample})
    merge_file=$(echo $sample | cut -d "." -f 1)
    
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/cutadapt/${merge_file}_trimmed.fastq.gz out/merged/${merge_file}.fastq.gz > log/cutadapt/${merge_file}.log
    
done




for file in out/cutadapt/*
do
    sid=$(basename ${file})
    mkdir -p out/star/$sid
    STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/cutadapt/${sid} --readFilesCommand gunzip -c --outFileNamePrefix  out/star/${sid}/

done 

echo 'alineamiento finalizado'


grep -i "total basepairs" log/cutadapt/* >> log/pipeline.log
grep -i "uniquely mapped reads %" out/star/*/Log.final.out >> log/pipeline.log
grep -i "% of reads mapped to multiple loci" out/star/*/Log.final.out >> log/pipeline.log
grep -i "% of reads mapped to too many loci" out/star/*/Log.final.out >> log/pipeline.log


